###
# Fichero de definicion del diagrama.
# Autor: Javier de Marco
###
require './Component.rb'
# Clase Diagrama. Define el diagrama en su totalidad como una serie de componentes
# dentro de un array. Se confiara en el orden de insercion para determinar donde esta
# el componente.
class Diagram
  # Constructor del diagrama
  def initialize()
    @components = Array.new
  end
  # Metodo de agregacion de componentes. Si es una actividad inicial no podra
  # haber componentes antes, si es una actividad final no podra haber componentes despues.
  def add_child(component)
    if component.is_a?(Activity)
      if component.is_initial() == true 
        if !@components.empty?
          raise RuntimeError,
          "La actividad #{component.description} es inicial y no es posible añadirla.", caller
        end
        @components.push(component)
        component.parent = self
      elsif component.is_final() == false
        @components.push(component)
        component.parent = self
      end
    elsif component.is_a?(Condition) || component.is_a?(Loop) || component.is_a?(Agrupation) || 
      component.is_a?(ConditionPath) || component.is_a?(LoopPath)
      @components.push(component)
      component.parent = self
    else
      raise RuntimeError,
          "No se pudo agregar el componente: #{component.description}", caller
    end
  end
  # Metodo de obtencion de un componente por indice
  def get_component(position)
    @components[position]
  end
  def num_components()
    @components.size()
  end
  # Metodo de obtencion del array de componentes
  def get_components()
    @components
  end
  # Metodo de impresion del diagrama para el formato plantUML
  def generate_uml(file_name="Diagram.uml")
    File.open("#{file_name}", "w") do |file|
      file.puts("@startuml")
      print_components(file)
      file.puts("@enduml")
    end
  end
  # Metodo de impresion de los componentes directamente asiciados con el diagrama como padre
  # Aquellos componentes que no tienen mas padre que el propio diagrama. Los componentes de alto nivel
  def print_components(file)
    column = 0
    for component in @components
      # Si es una activadad
      if component.is_a?(Activity)
        # Se imprime start si es el inicial
        if component.is_initial() == true
          print_column(file, column)
          file.puts("start")
        end
        print_column(file, column)
        # Se imprime la descripcion
        file.puts(":#{component.description};")
        # Se imprime stop si es final
        if component.is_final() == true
          print_column(file, column)
          file.puts("stop")
        end
      # Si es una condicion
      elsif component.is_a?(Condition)
        print_column(file, column)
        # Se imprime el primer camino como un if then, junto con todos los componentes hijos
        # que ese camino pueda tener
        file.puts("if (#{component.description}) then (#{component.get_child(0).description})")
        print_subcomponents(file, component.get_child(0).get_children(), column)
        # Se imprime cada siguiente camino como un else, junto con sus componente hijos
        for i in 1..component.get_children_size()
          if component.get_child(i) != nil
            print_column(file, column)
            file.puts("else (#{component.get_child(i).description})")
            print_subcomponents(file, component.get_child(i).get_children(), column)
          end
        end
        print_column(file, column)
        # Condicion de final de if cuando se acabaron los caminos
        file.puts("endif")
      # Si es una agrupacion
      elsif component.is_a?(Agrupation)
        print_column(file, column)
        # Se imprime su particularidad junto con la descripcion y entre llaves sus hijos
        file.puts("partition #{component.description} {")
        print_subcomponents(file, component.get_children(), column)
        print_column(file, column)
        file.puts("}")
      # Si es un bucle
      elsif component.is_a?(Loop)
        print_column(file, column)
        # Como solo tiene dos opciones se imprime el primero junto con sus hijos
        file.puts("while (#{component.description}) is (#{component.get_child(0).description})")
        print_subcomponents(file, component.get_child(0).get_children(), column)
        print_column(file, column)
        # Imprimiendo el final de bucle a continuacion
        file.puts("endwhile (#{component.get_child(1).description})")
        if component.get_child(1).get_children() != nil
          print_subcomponents(file, component.get_child(1).get_children(), column)
        end
      end
    end
  end

  def print_subcomponents(file, components, uppercolumn)
    column = uppercolumn + 1
    for component in components
      # Si es una activadad
      if component.is_a?(Activity)
        # Se imprime start si es el inicial
        if component.is_initial() == true
          print_column(file, column)
          file.puts("start")
        end
        print_column(file, column)
        # Se imprime la descripcion
        file.puts(":#{component.description};")
        # Se imprime stop si es final
        if component.is_final() == true
          print_column(file, column)
          file.puts("stop")
        end
      # Si es una condicion
      elsif component.is_a?(Condition)
        print_column(file, column)
        # Se imprime el primer camino como un if then, junto con todos los componentes hijos
        # que ese camino pueda tener
        file.puts("if (#{component.description}) then (#{component.get_child(0).description})")
        print_subcomponents(file, component.get_child(0).get_children(), column)
        # Se imprime cada siguiente camino como un else, junto con sus componente hijos
        for i in 1..component.get_children_size()
          if component.get_child(i) != nil
            print_column(file, column)
            file.puts("else (#{component.get_child(i).description})")
            if component.get_child(i).get_children() != nil
              print_subcomponents(file, component.get_child(i).get_children(), column)
            end
          end
        end
        print_column(file, column)
        # Condicion de final de if cuando se acabaron los caminos
        file.puts("endif")
      # Si es una agrupacion
      elsif component.is_a?(Agrupation)
        print_column(file, column)
        # Se imprime su particularidad junto con la descripcion y entre llaves sus hijos
        file.puts("partition #{component.description} {")
        print_subcomponents(file, component.get_children(), column)
        print_column(file, column)
        file.puts("}")
      # Si es un bucle
      elsif component.is_a?(Loop)
        print_column(file, column)
        # Como solo tiene dos opciones se imprime el primero junto con sus hijos
        file.puts("while (#{component.description}) is (#{component.get_child(0).description})")
        print_subcomponents(file, component.get_child(0).get_children(), column)
        print_column(file, column)
        # Imprimiendo el final de bucle a continuacion
        file.puts("endwhile (#{component.get_child(1).description})")
        if component.get_child(1).get_children() != nil
          print_subcomponents(file, component.get_child(1).get_children(), column)
        end
      end
    end
  end
  # Metodo de impresion de tabulaciones para imprimer en la columna correcta.
  def print_column(file, columns)
    for i in 0...columns
      file.print("\t")
    end
  end

  def check_diagram()
    if !@components[0].is_a?(Activity)
      raise RuntimeError,
        "El primer componente tiene que ser una actividad #{component.description}", caller
    end
    if @components[0].is_initial() == false
      raise RuntimeError,
      "El primer componente tiene que ser una actividad inicial #{component.description}", caller
    end
    for c in @components
      if !c.is_a?(Activity)
        for a in c.get_children()
          if a.is_a?(Activity) && a.is_final() == true && a != c.get_children()[-1]
            raise RuntimeError,
            "El componente #{a.description} es final y hay componentes despues", caller
          end
          if a.is_a?(Activity) && a.is_initial() == true
            raise RuntimeError,
            "El componente #{a.description} es inicial no es posible", caller
          end
          if a.is_a?(Loop) && a.get_children_size() != 2
            raise RuntimeError,
            "El componente #{a.description} esta mal definido, es un bucle que tiene que tener exactamente 2 caminos", caller
          end
          if a.is_a?(Condition) && a.get_children_size() == 1
            raise RuntimeError,
            "El componente #{a.description} esta mal definido, es un Condition que tiene que tener al menos 2 caminos", caller
          end
          check_subComponents(a)
        end 
      end
    end
  end

  def check_subComponents(a)
    for com in a.get_children()
      if com.is_a?(Activity) && com.is_final() == true && com != a.get_children()[-1]
        raise RuntimeError,
        "El componente #{com.description} es final y hay componentes despues", caller
      end
      if com.is_a?(Activity) && com.is_initial() == true
        raise RuntimeError,
        "El componente #{com.description} es inicial no es posible", caller
      end
      if com.is_a?(Loop) && com.get_children_size() != 2
        raise RuntimeError,
        "El componente #{com.description} esta mal definido, es un bucle que tiene que tener exactamente 2 caminos", caller
      end
      if com.is_a?(Condition) && com.get_children_size() == 1
        raise RuntimeError,
        "El componente #{com.description} esta mal definido, es un Conditional que tiene que tener al menos 2 caminos", caller
      end
      if !com.is_a?(Activity)
        check_subComponents(com)
      end
    end
  end
  attr_reader :components
end