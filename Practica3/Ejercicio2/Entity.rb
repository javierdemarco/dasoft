# Clase entidad que representa los modelos de datos del cahtbot
class Entity
  #Constructoe de la entidad
  def initialize(name)
    @name = name
    @properties = Array.new
    @create_commands = Array.new
    @delete_commands = Array.new
  end
  # Metodo de captacion de la palabra clave propertie y su posterior creacion
  def property(name, type)
    p = Property.new(name, type)
    @properties.push(p)
    p
  end
  # Metodo de captacion de comandos para la creacion de una entidad
  def create_entity_commands(frase)
    @create_commands.push(frase)
  end
  # Metodo de captacion de comandos para la eliminacion de una entidad
  def delete_entity_commands(frase)
    @delete_commands.push(frase)
  end
end
#Clase propiedad dentro de una entidad
class Property
  # Constructor de una propiedad
  def initialize(name, type)
    @name = name
    @type = type
    @required = false
  end
  #Metodo que indica si una propiedad es requerida
  def required
    @required = true
  end
end
